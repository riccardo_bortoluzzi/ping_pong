#!.venv/bin/python3

#script per gestire il gioco del ping pong

from threading import Thread
import keyboard, time, math, random

try:
	import Tkinter as tk
except ImportError:
	import tkinter as tk

try:
	import ttk
	py3 = False
except ImportError:
	import tkinter.ttk as ttk
	py3 = True

##############################################################################################################################
#costanti

const_key_up = 'up'
const_key_down = 'down'
const_giocatore_umano = 'U'
const_giocatore_pc = 'C'

const_velocita_iniziale_pallina = 100 #rappresentata in caselle/s
const_incremento_velocita_pallina = 0.1 #facciamo che si incrementa in percentuale ad ogni rimbalzo 
const_dim_racchetta = 101
const_minimo_tempo_spostamento = 0.005
const_dim_misura_base = 20
const_raggio_pallina = 5
const_lista_angoli_da_evitare = (0, math.pi/2, math.pi, (3*math.pi)/2, 2*math.pi)

const_char_pallina = '*'
const_chat_racchetta = '|'
const_char_bordo = '~'



#####################################################################################################################################
#classi

class giocatore:
	#classe per gestire il giocatore
	tipo = None
	tasto_su = None
	tasto_giu = None
	tempo_ultima_mossa = None
	posizione_verticale = None
	
	def __init__(self, tipo=const_giocatore_pc, tasto_su=const_key_up, tasto_giu=const_key_down):
		#per inizializzare assegno gli attributi
		self.tipo = tipo
		self.tasto_su = tasto_su
		self.tasto_giu = tasto_giu
		self.tempo_ultima_mossa = time.time()
		self.posizione_verticale = calcola_posizione_verticale_iniziale()
	
	def esegui_mosse_giocatore(self, oggetto_pallina):
		#funzione che decide se un giocatore e un pc o un umano
		if self.tipo == const_giocatore_pc:
			self.esegui_mosse_giocatore_computer(oggetto_pallina = oggetto_pallina)
		elif self.tipo == const_giocatore_umano:
			self.esegui_mosse_giocatore_umano(oggetto_pallina=oggetto_pallina)
	
	def esegui_mosse_giocatore_umano(self, oggetto_pallina):
		#metodo per eseguire tutte le mosse per il giocatore umano
		while oggetto_pallina.flag_superato_linea_difensiva == False:
			#devo vedere se l'utente preme il tasto su o giu
			if keyboard.is_pressed(self.tasto_su):
				self.muovi_su(oggetto_pallina = oggetto_pallina)
			elif keyboard.is_pressed(self.tasto_giu):
				self.muovi_giu(oggetto_pallina = oggetto_pallina)
	
	def esegui_mosse_giocatore_computer(self, oggetto_pallina):
		#funzione per eseguire la mossa del computer
		while oggetto_pallina.flag_superato_linea_difensiva == False:
			#devo vedere se si trova sopra o sotto la pallina
			differenza = oggetto_pallina.y - self.posizione_verticale
			if differenza > 0:
				self.muovi_su(oggetto_pallina = oggetto_pallina)
			elif differenza <0:
				self.muovi_giu(oggetto_pallina = oggetto_pallina)
		
	def muovi_su(self, oggetto_pallina):
		#funzione che verifica tutte le condizioni se puo muovere su
		#primo controllo sul tempo
		if (time.time() - self.tempo_ultima_mossa) > const_minimo_tempo_spostamento:
			#verifico se posso muovermi verso l'alto ovvero se ho un numero di caselle adeguato
			if (self.posizione_verticale + gl_dim_semiracchetta) < gl_dim_v:
				#verifico se posso muovermi in base al flag della pallina
				if oggetto_pallina.flag_superato_linea_difensiva == False:
					#aumento di 1 la posizione
					self.posizione_verticale += 1
					self.tempo_ultima_mossa = time.time()
				else:
					pass
			else:
				pass
		else:
			pass
	
	def muovi_giu(self, oggetto_pallina):
		#funzione che verifica tutte le condizioni se puo muovere su
		#primo controllo sul tempo
		if (time.time() - self.tempo_ultima_mossa) > const_minimo_tempo_spostamento:
			#verifico se posso muovermi verso l'alto ovvero se ho un numero di caselle adeguato
			if (self.posizione_verticale - gl_dim_semiracchetta) >= 0:
				#verifico se posso muovermi in base al flag della pallina
				if oggetto_pallina.flag_superato_linea_difensiva == False:
					#aumento di 1 la posizione
					self.posizione_verticale -= 1
					self.tempo_ultima_mossa = time.time()
				else:
					pass
			else:
				pass
		else:
			pass
		
class pallina:
	#classe per rappresentare la pallina
	x = None
	y = None
	angolo = None
	velocita=None
	flag_superato_linea_difensiva = None
	#tempo_ultima_misurazione = None
	
	incremento_x = None #poi puo venire calcolato con il coseno
	incremento_y = None #poi puo venire calcolato con il seno
	
	def __init__(self):
		#ho deciso di non riutilizzare l'oggetto pallina
		#self.calcola_posizione_iniziale_con_virgola()
		self.x = gl_dim_o/2
		self.y = gl_dim_v/2
		self.velocita = const_velocita_iniziale_pallina
		self.flag_superato_linea_difensiva = False
		#calcolo l'angolo come angolo casuale
		self.angolo = random.uniform(0, 2*math.pi)
		while self.angolo in const_lista_angoli_da_evitare:
			self.angolo = random.uniform(0, 2*math.pi)
			
	def aumenta_velocita(self):
		#metodo per aumentare la velocita attuale
		self.velocita = self.velocita * (1+const_incremento_velocita_pallina)
		print('velocita incrementata', self.velocita)
		
	def varia_direzione(self, giocatore_sx, giocatore_dx):
		#funzione che verifica se e necessario variare direzione alla pallina 
		#verifico prima le direzioni che sbattono sui difensori, poi sui muri
		if (self.x <= const_raggio_pallina) and ( abs( giocatore_sx.posizione_verticale - self.y ) <= (gl_dim_semiracchetta+const_raggio_pallina) ) and ((math.pi/2) <= self.angolo <= (3*math.pi/2)): #sbatto a sinistra
			#deve variare
			self.angolo = ( math.pi - self.angolo )%(2*math.pi)
			#modifico la velocita
			print('sbatto sx')
			self.aumenta_velocita()
		elif ((self.x + const_raggio_pallina) >= (gl_dim_o-1)) and ( abs( giocatore_dx.posizione_verticale - self.y ) <= (gl_dim_semiracchetta + const_raggio_pallina) ) and ((0<= self.angolo <= (math.pi/2)) or ((3*math.pi/2)<=self.angolo<=2*math.pi) ): #sbatto a destra
			#deve variare
			self.angolo = ( math.pi - self.angolo )%(2*math.pi)
			#modifico la velocita
			print('sbatto destra')
			self.aumenta_velocita()
		elif ( const_raggio_pallina<= self.x <(gl_dim_o - const_raggio_pallina) ) and (self.y <= const_raggio_pallina ) and (math.pi<= self.angolo <= 2*math.pi): #sbatto basso
			#deve variare
			self.angolo = (- self.angolo )%(2*math.pi)
			#modifico la velocita
			print('sbatto basso')
			self.aumenta_velocita()
		elif ( const_raggio_pallina<= self.x <(gl_dim_o - const_raggio_pallina) ) and (self.y >= (gl_dim_v - const_raggio_pallina) ) and (0<=self.angolo <= math.pi): #sbatto alto
			#deve variare
			self.angolo = (- self.angolo )%(2*math.pi)
			#modifico la velocita
			print('sbatto alto')
			self.aumenta_velocita()
		else:
			pass
	
	def muovi_pallina(self, tempo_mossa_precedente):
		#metodo per muovere la pallina
		diff_tempo = time.time() - tempo_mossa_precedente
		self.x += self.velocita*( math.cos(self.angolo) )*diff_tempo
		self.y += self.velocita*( math.sin(self.angolo) )*diff_tempo
	
	def esegui_mosse_pallina(self, giocatore_sx, giocatore_dx ):
		#metodo per far muovere la pallina fino a quando non fa un punto un giocatore
		#inizializzo il tempo
		tempo_ultima_mossa = time.time()
		while self.flag_superato_linea_difensiva == False:
			#muovo
			self.muovi_pallina(tempo_mossa_precedente = tempo_ultima_mossa)
			#aggiorno il tempo
			tempo_ultima_mossa = time.time()
			#vario la direzione
			self.varia_direzione(giocatore_sx = giocatore_sx, giocatore_dx=giocatore_dx)
			#verifico se ha ragiunto l'bierttivo
			self.verifica_punto_segnato()
	
	def verifica_punto_segnato(self):
		#metodo per verificare se la pallina e arrivata oltre una linea difensiva
		if -const_raggio_pallina<= self.x < (gl_dim_o + const_raggio_pallina):
			self.flag_superato_linea_difensiva = False
		else:
			print('Punto', self.x)
			self.flag_superato_linea_difensiva = True
	
class PingPong:
	
	top = None
	barra_sopra = None
	barra_sotto = None
	racchetta_sx = None
	racchetta_dx = None
	pallina = None
	punteggio_sx = None
	punteggio_dx = None
	var_punteggio_sx = None
	var_punteggio_dx = None
	
	def __init__(self, top=None):
		'''This class configures and populates the toplevel window.
		   top is the toplevel containing window.'''
		_bgcolor = '#d9d9d9'  # X11 color: 'gray85'
		_fgcolor = '#000000'  # X11 color: 'black'
		_compcolor = '#d9d9d9' # X11 color: 'gray85'
		_ana1color = '#d9d9d9' # X11 color: 'gray85'
		_ana2color = '#ececec' # Closest X11 color: 'gray92'
		
		self.top = top
		
		#top.geometry("600x450+377+131")
		top.geometry('%dx%d'%(gl_dim_orizz_finestra, gl_dim_vert_finestra))
		top.minsize(gl_dim_orizz_finestra, gl_dim_vert_finestra)
		top.maxsize(gl_dim_orizz_finestra, gl_dim_vert_finestra)
		#top.minsize(1, 1)
		#top.maxsize(1265, 770)
		#top.resizable(1, 1)
		top.title("Ping Pong")

		self.barra_sopra = tk.Canvas(top)
		#self.barra_sopra.place(relx=0.183, rely=0.044, relheight=0.069
		#		, relwidth=0.752)
		self.barra_sopra.place(relx=(const_dim_misura_base/gl_dim_orizz_finestra), rely=0, relheight=(const_dim_misura_base/gl_dim_vert_finestra), relwidth=(gl_dim_o/gl_dim_orizz_finestra))
		self.barra_sopra.configure(borderwidth="0")
		self.barra_sopra.configure(background="#000000")
		#self.barra_sopra.configure(cursor="fleur")
		#self.barra_sopra.configure(relief="ridge")
		#self.barra_sopra.configure(selectbackground="#c4c4c4")

		self.barra_sotto = tk.Canvas(top)
		#self.barra_sotto.place(relx=0.15, rely=0.6, relheight=0.069
		#		, relwidth=0.752)
		self.barra_sotto.place(relx=(const_dim_misura_base/gl_dim_orizz_finestra), rely=(const_dim_misura_base+gl_dim_v)/gl_dim_vert_finestra, relheight=const_dim_misura_base/gl_dim_vert_finestra, relwidth=gl_dim_o/gl_dim_orizz_finestra)
		self.barra_sotto.configure(borderwidth="0")
		self.barra_sotto.configure(background="#000000")
		#self.barra_sotto.configure(relief="ridge")
		#self.barra_sotto.configure(selectbackground="#c4c4c4")

		self.racchetta_sx = tk.Canvas(top)
		#self.racchetta_sx.place(relx=0.067, rely=0.067, relheight=0.358
		#		, relwidth=0.035)
		self.racchetta_sx.place(relx=0, rely=const_dim_misura_base/gl_dim_vert_finestra, relheight=const_dim_racchetta/gl_dim_vert_finestra, relwidth=const_dim_misura_base/gl_dim_orizz_finestra)
		self.racchetta_sx.configure(background="#000000")
		#self.racchetta_sx.configure(relief="ridge")
		#self.racchetta_sx.configure(selectbackground="#c4c4c4")
		self.racchetta_sx.configure(borderwidth="0")

		self.racchetta_dx = tk.Canvas(top)
		#self.racchetta_dx.place(relx=0.9, rely=0.133, relheight=0.269
		#		, relwidth=0.035)
		self.racchetta_dx.place(relx=(const_dim_misura_base+gl_dim_o)/gl_dim_orizz_finestra, rely=const_dim_misura_base/gl_dim_vert_finestra, relheight=const_dim_racchetta/gl_dim_vert_finestra, relwidth=const_dim_misura_base/gl_dim_orizz_finestra)
		self.racchetta_dx.configure(background="#000000")
		#self.racchetta_dx.configure(relief="ridge")
		#self.racchetta_dx.configure(selectbackground="#c4c4c4")
		self.racchetta_dx.configure(borderwidth="0")

		self.pallina = tk.Canvas(top)
		self.pallina.place(relx=(round(gl_dim_o/2))/gl_dim_orizz_finestra, rely=(round(gl_dim_v/2))/gl_dim_vert_finestra, height=2*const_raggio_pallina, width=2*const_raggio_pallina)
		self.pallina.configure(background="#000000")
		#self.pallina.place(relx=0.45, rely=0.289, relheight=0.091
		#		, relwidth=0.068)
		#self.pallina.create_(0,0, 2*const_raggio_pallina, 2*const_raggio_pallina, fill='#000000')#fill='#000000'
		#self.pallina.configure(relief="ridge")
		#self.pallina.configure(selectbackground="#c4c4c4")
		#self.pallina.configure(background="")
		self.pallina.configure(borderwidth="0")

		self.punteggio_sx = tk.Label(top)
		#self.punteggio_sx.place(relx=0.033, rely=0.8, height=18, width=67)
		self.punteggio_sx.place(relx=0, rely=(3*const_dim_misura_base + gl_dim_v)/gl_dim_vert_finestra)
		self.var_punteggio_sx = tk.StringVar()
		self.var_punteggio_sx.set('0')
		#self.punteggio_sx.configure(textvariable=esempio_grafica_support.var_punteggio_sx)
		self.punteggio_sx.configure(textvariable=self.var_punteggio_sx)

		self.punteggio_dx = tk.Label(top)
		#self.punteggio_dx.place(relx=0.75, rely=0.844, height=18, width=107)
		self.punteggio_dx.place(relx=(const_dim_misura_base+gl_dim_o)/gl_dim_orizz_finestra, rely=(3*const_dim_misura_base + gl_dim_v)/gl_dim_vert_finestra )
		self.var_punteggio_dx = tk.StringVar()
		self.var_punteggio_dx.set('0')
		#self.punteggio_dx.configure(textvariable=esempio_grafica_support.var_punteggio_dx)
		self.punteggio_dx.configure(textvariable=self.var_punteggio_dx)
			
	
######################################################################################################################################
#funzioni

def calcola_posizione_verticale_iniziale():
	#funzione per calcoalre la posizione verticale iniziale
	return (gl_dim_v-1)/2 
	
def calcola_posizione_orizzontale_iniziale():
	#funzione per calcolare il centro in orizzontale
	return  ( (gl_dim_o-1)/2 )

def valorizza_variabili_globali():
	#funzione per valorizzare le variabili globali all'inizio
	global gl_dim_semiracchetta,gl_celle_griglia_popolate, gl_dim_orizz_finestra,gl_dim_vert_finestra
	gl_dim_semiracchetta = ( (const_dim_racchetta-1)/2 )
	gl_celle_griglia_popolate = []
	gl_dim_orizz_finestra = 2*const_dim_misura_base + gl_dim_o
	gl_dim_vert_finestra = 4*const_dim_misura_base + gl_dim_v

def avvia_gioco():
	#funzione per creare la grafica di base 
	root = tk.Tk()
	pingpong = PingPong(root)
	#nuovo_thread = Thread(target=root.mainloop, args=(), daemon=True)
	#nuovo_thread.start()
	#nuovo_thread.join()
	#print('in parallelo')
	nuovo_thread = Thread(target=gioca_lista_partite, args=(pingpong,), daemon=True)
	nuovo_thread.start()
	root.mainloop()
	#return pingpong
	
def aggiorna_grafica(oggetto_pingpong, giocatore_sx, giocatore_dx, oggetto_pallina):
	#funzione per aggiornare la grafica 
	#aggiorno la posizione del giocatore sx
	y = gl_dim_v - giocatore_sx.posizione_verticale - gl_dim_semiracchetta + const_dim_misura_base
	oggetto_pingpong.racchetta_sx.place(rely=y/gl_dim_vert_finestra)
	#aggiorno il giocatore a dx
	y = gl_dim_v - giocatore_dx.posizione_verticale - gl_dim_semiracchetta + const_dim_misura_base
	oggetto_pingpong.racchetta_dx.place(rely=y/gl_dim_vert_finestra)
	#comincio aggiornando la pallina
	x = oggetto_pallina.x - 1*const_raggio_pallina + const_dim_misura_base
	y = gl_dim_v - oggetto_pallina.y - 1*const_raggio_pallina + const_dim_misura_base
	oggetto_pingpong.pallina.place(relx=x/gl_dim_orizz_finestra, rely=y/gl_dim_vert_finestra)

def gioca_partita(oggetto_pingpong):
	#funzione per giocare una partita
	#creo gli oggetti
	oggetto_pallina = pallina()
	if gl_numero_giocatori == 0:
		giocatore_sx = giocatore(tipo=const_giocatore_pc, tasto_su=const_key_up, tasto_giu=const_key_down)
		giocatore_dx = giocatore(tipo=const_giocatore_pc, tasto_su=const_key_up, tasto_giu=const_key_down)
	elif gl_numero_giocatori == 1:
		giocatore_sx = giocatore(tipo=const_giocatore_umano, tasto_su=const_key_up, tasto_giu=const_key_down)
		giocatore_dx = giocatore(tipo=const_giocatore_pc, tasto_su=const_key_up, tasto_giu=const_key_down)
	elif gl_numero_giocatori == 2:
		giocatore_sx = giocatore(tipo=const_giocatore_umano, tasto_su='w', tasto_giu='s')
		giocatore_dx = giocatore(tipo=const_giocatore_umano, tasto_su=const_key_up, tasto_giu=const_key_down)
	else:
		raise Exception('Numero giocatori umani non valido')
	#ora aggiorno la grafica
	aggiorna_grafica(oggetto_pingpong = oggetto_pingpong, giocatore_sx = giocatore_sx, giocatore_dx = giocatore_dx, oggetto_pallina=oggetto_pallina)
	#ora attendo 5 secondi prima che la partita cominci
	time.sleep(5)
	print('Inizio gioco')
	#preparo i thread per i vari giocatori
	thread_giocatore_sx = Thread(target=giocatore_sx.esegui_mosse_giocatore, args=(oggetto_pallina,), daemon=True)
	thread_giocatore_dx = Thread(target=giocatore_dx.esegui_mosse_giocatore, args=(oggetto_pallina,), daemon=True)
	thread_pallina = Thread(target=oggetto_pallina.esegui_mosse_pallina, args=(giocatore_sx, giocatore_dx), daemon=True)
	#faccio partire i thread
	thread_giocatore_sx.start()
	thread_giocatore_dx.start()
	thread_pallina.start()
	#eseguo il while
	while oggetto_pallina.flag_superato_linea_difensiva == False:
		#print('dentro')
		aggiorna_grafica(oggetto_pingpong = oggetto_pingpong, giocatore_sx = giocatore_sx, giocatore_dx = giocatore_dx, oggetto_pallina=oggetto_pallina)
	print('Fine gioco')
	aggiorna_grafica(oggetto_pingpong = oggetto_pingpong, giocatore_sx = giocatore_sx, giocatore_dx = giocatore_dx, oggetto_pallina=oggetto_pallina)
	#decido a chi assegnare il punteggio
	if oggetto_pallina.x < (gl_dim_o/2):
		var_punteggio = oggetto_pingpong.var_punteggio_dx
	else:
		var_punteggio = oggetto_pingpong.var_punteggio_sx
	valore_punteggio = int( var_punteggio.get() ) +1
	var_punteggio.set(str(valore_punteggio))
	#aspetto 5 secondi
	time.sleep(5)
	
def gioca_lista_partite(oggetto_pingpong):
	for i in range(gl_numero_partite):
		print('Inizio partita', i+1)
		gioca_partita(oggetto_pingpong = oggetto_pingpong)
		print('Fine partita', i+1)

#######################################################################################################################################
#main

def main():
	valorizza_variabili_globali()
	#oggetto_pingpong = genera_grafica_di_base()
	#gioca_partita(oggetto_pingpong)
	avvia_gioco()
	
########################################################################################################################################
#variabili globali

gl_dim_semiracchetta = None
gl_celle_griglia_popolate = None

gl_dim_orizz_finestra = None
gl_dim_vert_finestra = None

########################################################################################################################################
#parametri

gl_dim_v = 400
gl_dim_o = 600

gl_numero_giocatori = 1

gl_numero_partite = 5

#########################################################################################################################################

if __name__ == '__main__':
	main()
