#! /usr/bin/env python
#  -*- coding: utf-8 -*-
#
# Support module generated by PAGE version 5.0.3
#  in conjunction with Tcl version 8.6
#    Jun 03, 2020 10:19:50 AM CEST  platform: Linux

import sys

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

def set_Tk_var():
    global var_punteggio_sx
    var_punteggio_sx = tk.StringVar()
    var_punteggio_sx.set('')
    global var_punteggio_dx
    var_punteggio_dx = tk.StringVar()
    var_punteggio_dx.set('')

def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

if __name__ == '__main__':
    import esempio_grafica
    esempio_grafica.vp_start_gui()




